<?php
namespace Astartsky\SitemapGenerator\UrlEntryProcessor;

use Astartsky\SitemapGenerator\UrlEntry;

interface UrlEntryProcessorInterface
{
    /**
     * @param UrlEntry $urlEntry
     * @return UrlEntry
     */
    public function process(UrlEntry $urlEntry);
}