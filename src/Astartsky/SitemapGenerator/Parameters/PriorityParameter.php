<?php
namespace Astartsky\SitemapGenerator\Parameters;

use Astartsky\SitemapGenerator\SitemapGeneratorException;

class PriorityParameter implements ParameterInterface
{
    protected $priority;

    /**
     * @param string $priority
     * @throws SitemapGeneratorException
     */
    public function __construct($priority)
    {
        if (false === $this->valid($priority)) {
            throw new SitemapGeneratorException("Incorrect priority", 0, null, array("priority" => $priority));
        }

        $this->priority = $priority;
    }

    /**
     * @param float $priority
     * @return bool
     */
    protected function valid($priority)
    {
        return $priority >= 0.0 && $priority <= 1.0;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return (string) $this->priority;
    }

    /**
     * @return string
     */
    public function getProcessedValue()
    {
        return (string) $this->priority;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "priority";
    }
}