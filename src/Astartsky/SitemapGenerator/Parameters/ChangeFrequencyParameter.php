<?php
namespace Astartsky\SitemapGenerator\Parameters;

use Astartsky\SitemapGenerator\SitemapGeneratorException;

class ChangeFrequencyParameter implements ParameterInterface
{
    const ALWAYS = "always";
    const HOURLY = "hourly";
    const DAILY = "daily";
    const WEEKLY = "weekly";
    const MONTHLY = "monthly";
    const YEARLY = "yearly";
    const NEVER = "never";

    protected $frequency;

    /**
     * @param string $frequency
     * @throws SitemapGeneratorException
     */
    public function __construct($frequency)
    {
        if (false === $this->valid($frequency)) {
            throw new SitemapGeneratorException("Unknown `frequency` parameter", 0, null, array("frequency" => $frequency));
        }

        $this->frequency = $frequency;
    }

    /**
     * @param string $frequency
     * @return bool
     */
    protected function valid($frequency)
    {
        return in_array($frequency, array(self::ALWAYS, self::HOURLY, self::DAILY, self::WEEKLY, self::MONTHLY, self::YEARLY, self::NEVER));
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return (string) $this->frequency;
    }

    /**
     * @return string
     */
    public function getProcessedValue()
    {
        return (string) $this->frequency;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "changefreq";
    }
}