<?php
namespace Astartsky\SitemapGenerator\Job;

use Astartsky\SitemapGenerator\SitemapGenerator;

interface JobInterface
{
    /**
     * @param SitemapGenerator $generator
     * @return void
     */
    public function run(SitemapGenerator $generator);
}