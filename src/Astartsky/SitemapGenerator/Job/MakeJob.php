<?php
namespace Astartsky\SitemapGenerator\Job;

use Astartsky\SitemapGenerator\SitemapGenerator;
use Astartsky\SitemapGenerator\SitemapGeneratorException;
use Astartsky\SitemapGenerator\UrlEntryProcessor\UrlEntryProcessorInterface;
use Astartsky\SitemapGenerator\UrlEntry;

class MakeJob implements JobInterface
{
    public function run(SitemapGenerator $generator)
    {
        $writer = $generator->getWriter();
        if (null === $writer) {
            throw new SitemapGeneratorException("Writer is not set");
        }

        $renderer = $generator->getRenderer();
        if (null === $renderer) {
            throw new SitemapGeneratorException("Renderer is not set");
        }

        $writer->open();

        if ($xml = $renderer->renderHeader()) {
            $writer->append($xml);
        }

        /** @var \Iterator $iterator */
        foreach ($generator->getUrlEntryIterators() as $iterator) {

            /** @var UrlEntry $urlEntry */
            foreach ($iterator as $urlEntry) {

                if (false === $urlEntry instanceof UrlEntry) {
                    throw new SitemapGeneratorException("Iterator must return UrlEntry", 0, null, array("got" => $urlEntry));
                }

                /** @var UrlEntryProcessorInterface $urlEntryProcessor */
                foreach ($generator->getUrlEntryProcessors() as $urlEntryProcessor) {
                    $urlEntry = $urlEntryProcessor->process($urlEntry);
                }

                if ($xml = $renderer->render($urlEntry)) {
                    $writer->append($xml);
                }
            }
        }

        if ($xml = $renderer->renderFooter()) {
            $writer->append($xml);
        }

        $writer->close();
    }
}