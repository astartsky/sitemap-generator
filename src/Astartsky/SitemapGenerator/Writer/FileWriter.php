<?php
namespace Astartsky\SitemapGenerator\Writer;

use Astartsky\SitemapGenerator\SitemapGeneratorException;

class FileWriter implements WriterInterface
{
    /** @var resource */
    protected $handler;

    /** @var string */
    protected $filename;

    /**
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @throws SitemapGeneratorException
     */
    public function open()
    {
        $this->handler = @fopen($this->filename, 'w');
        if (!$this->handler) {
            throw new SitemapGeneratorException("Can't open file", 0, null, array("file" => $this->filename));
        }
    }

    /**
     * @param string $text
     */
    public function append($text)
    {
        fwrite($this->handler, $text);
    }

    /**
     * @throws SitemapGeneratorException
     */
    public function close()
    {
        $result = fclose($this->handler);
        if (false === $result) {
            throw new SitemapGeneratorException("Can't close file", 0, null, array("file" => $this->filename));
        }
    }
}