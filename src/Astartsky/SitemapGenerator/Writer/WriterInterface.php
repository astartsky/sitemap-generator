<?php
namespace Astartsky\SitemapGenerator\Writer;

use Astartsky\SitemapGenerator\SitemapGeneratorException;

interface WriterInterface
{
    /**
     * @throws SitemapGeneratorException
     */
    public function open();

    /**
     * @param string $text
     * @throws SitemapGeneratorException
     */
    public function append($text);

    /**
     * @throws SitemapGeneratorException
     */
    public function close();
}