<?php
namespace Astartsky\SitemapGenerator;

use Astartsky\SitemapGenerator\Parameters\ParameterInterface;

class UrlEntry
{
    protected $parameters = array();

    /**
     * @param ParameterInterface $parameter
     * @return $this
     */
    public function addParameter(ParameterInterface $parameter)
    {
        $this->parameters[$parameter->getKey()] = $parameter;

        return $this;
    }

    /**
     * @param string $key
     * @return $this
     * @throws SitemapGeneratorException
     */
    public function removeParameter($key)
    {
        if (false === isset($this->parameters[$key])) {
            throw new SitemapGeneratorException("Parameter is not exist", 0, null, array("key" => $key));
        }

        unset($this->parameters[$key]);

        return $this;
    }

    /**
     * @param string $key
     * @return ParameterInterface
     * @throws SitemapGeneratorException
     */
    public function getParameter($key)
    {
        if (false === isset($this->parameters[$key])) {
            throw new SitemapGeneratorException("Parameter is not exist", 0, null, array("key" => $key));
        }

        return $this->parameters[$key];
    }

    /**
     * @return ParameterInterface[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}