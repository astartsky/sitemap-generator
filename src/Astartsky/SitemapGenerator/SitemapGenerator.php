<?php

namespace Astartsky\SitemapGenerator;

use Astartsky\SitemapGenerator\Renderer\RendererInterface;
use Astartsky\SitemapGenerator\UrlEntryProcessor\UrlEntryProcessorInterface;
use Astartsky\SitemapGenerator\Writer\WriterInterface;

class SitemapGenerator
{
    /**
     * @var \Iterator[]
     */
    protected $urlEntryIterators = array();

    /**
     * @var UrlEntryProcessorInterface[]
     */
    protected $urlEntryProcessors = array();

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @var  WriterInterface
     */
    protected $writer;

    /**
     * @param \Iterator $iterator
     * @param string $key
     * @return $this
     */
    public function addUrlEntryIterator(\Iterator $iterator, $key = null)
    {
        if (null === $key) {
            $this->urlEntryIterators[] = $iterator;
        } else {
            $this->urlEntryIterators[$key] = $iterator;
        }

        return $this;
    }

    /**
     * @param string $key
     * @throws SitemapGeneratorException
     * @return $this
     */
    public function removeUrlEntryIterator($key)
    {
        if (false === isset($this->urlEntryIterators[$key])) {
            throw new SitemapGeneratorException("Url entry iterator is not exist", 0, null, array("key" => $key));
        }

        unset($this->urlEntryIterators[$key]);

        return $this;
    }

    /**
     * @return \Iterator[]
     */
    public function getUrlEntryIterators()
    {
        return $this->urlEntryIterators;
    }

    /**
     * @param UrlEntryProcessorInterface $sourcePreprocessor
     * @param string $key
     * @return $this
     */
    public function addUrlEntryProcessor(UrlEntryProcessorInterface $sourcePreprocessor, $key = null)
    {
        $this->urlEntryProcessors[$key] = $sourcePreprocessor;

        return $this;
    }

    /**
     * @param string $key
     * @return $this
     * @throws SitemapGeneratorException
     */
    public function removeUrlEntryProcessor($key)
    {
        if (false === isset($this->urlEntryProcessors[$key])) {
            throw new SitemapGeneratorException("Url entry preprocessor is not exist", 0, null, array("key" => $key));
        }

        unset($this->urlEntryProcessors[$key]);

        return $this;
    }

    /**
     * @return UrlEntryProcessorInterface[]
     */
    public function getUrlEntryProcessors()
    {
        return $this->urlEntryProcessors;
    }

    /**
     * @param RendererInterface $renderer
     * @return $this
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;

        return $this;
    }

    /**
     * @return RendererInterface
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param WriterInterface $writer
     * @return $this
     */
    public function setWriter(WriterInterface $writer)
    {
        $this->writer = $writer;

        return $this;
    }

    /**
     * @return WriterInterface
     */
    public function getWriter()
    {
        return $this->writer;
    }
}