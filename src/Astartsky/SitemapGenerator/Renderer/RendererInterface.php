<?php
namespace Astartsky\SitemapGenerator\Renderer;

use Astartsky\SitemapGenerator\UrlEntry;

interface RendererInterface
{
    /**
     * @return string
     */
    public function renderHeader();

    /**
     * @param UrlEntry $urlEntry
     * @return string
     */
    public function render(UrlEntry $urlEntry);

    /**
     * @return string
     */
    public function renderFooter();
}