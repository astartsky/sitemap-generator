<?php
namespace Astartsky\SitemapGenerator\Renderer;

use Astartsky\SitemapGenerator\Parameters\ParameterInterface;
use Astartsky\SitemapGenerator\UrlEntry;

class Renderer implements RendererInterface
{
    /**
     * @return string
     */
    public function renderHeader()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
    }

    /**
     * @param UrlEntry $urlEntry
     * @return string
     */
    public function render(UrlEntry $urlEntry)
    {
        $xml  = "<url>\n";

        /** @var ParameterInterface $parameter */
        foreach ($urlEntry->getParameters() as $parameter) {
            $xml .= "   <{$parameter->getKey()}>{$parameter->getProcessedValue()}</{$parameter->getKey()}>\n";
        }

        $xml .= "</url>\n";

        return $xml;
    }

    /**
     * @return string
     */
    public function renderFooter()
    {
        return '</urlset>';
    }
}