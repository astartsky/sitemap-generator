<?php
namespace Astartsky\SitemapGenerator;

class SitemapGeneratorException extends \Exception
{
    protected $context = array();

    /**
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     * @param string[] $context
     */
    public function __construct($message = "", $code = 0, \Exception $previous = null, $context = array())
    {
        $this->context = $context;
        parent::__construct($message, $code, $previous = null);
    }

    /**
     * @return string[]
     */
    public function getContext()
    {
        return $this->context;
    }
}